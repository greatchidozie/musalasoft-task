# Musala-Soft Task : Drone Delivery Service.

This System is a new and disruptive means of goods transportation:
It is set to challenge the traditional method of transporting goods(Logistics).
The service will help delivery of small items that are (urgently) needed in locations with difficult access.

## How to Run The Service

This application is packaged as a jar which has Tomcat embedded. No Tomcat installation is necessary.
You run it using the java -jar command.

    1. Clone this repository with the url below
``` 
   git clone https://gitlab.com/greatchidozie/musalasoft-task.git
```
    2. Make sure you have JDK 17 or higher and Maven 3.x
    3. You can build the project and run the tests by running mvn clean package
    3. Once successfully built, you can run the service with the command below:

```
java -jar -Dspring.profiles.active=test target/musalasoft-task-0.0.1-SNAPSHOT.jar
```
    4. Check the log file file to make sure no exceptions are thrown.

## About the Service

This is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone transportation**.
Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication,
the drone has the potential to leapfrog traditional transportation infrastructure.


## Here are the endpoints you can call:

```http
  POST http://localhost:8080/register-drone
  POST http://localhost:8080/load-drone/{drone-serial-number}
  GET http://localhost:8080/check-items/{drone-serial-number}
  GET http://localhost:8080/available-drones
  GET http://localhost:8080/battery-level/{drone-serial-number}
```

## Register Drone Resources:
POST http://localhost:8080/register-drone
```
    {
        "serialNumber": "1234567890",
        "model": "Lightweight",
        "weight": 500,
        "batteryCapacity": 0,
        "state": "IDLE"
    }
    
    ResponseBody : {
        "id": 1,
        "serialNumber": "21349902433",
        "model": "Lightweight",
        "weight": 450,
        "batteryCapacity": 100,
        "state": "IDLE",
        "medications": null
    }
    
    ResponseCode: HTTP 201
```

## Load a Drone Resources:
POST http://localhost:8080/load-drone/{drone-serial-number}
```
    {
        "name": "MEDICATION_NAME",
        "weight": 30,
        "code": "MEDICATIONCODE",
        "image": "imageurl"
    }
    The above resource can be used to load a drone 
    by simply adding it into a .json file.
    The inserted collection shows how it's used.
    
    ResponseBody : {
        "id": 1,
        "serialNumber": "2499023499863",
        "model": "Lightweight",
        "weight": 500,
        "batteryCapacity": 80,
        "state": "LOADED",
        "medications": [
            {
                "id": 1,
                "name": "MALARIA_223",
                "weight": 30,
                "code": "CODEFORTHIS",
                "image": "imageurl.com"
            }
        ]
    }
    
    ResponseCode: HTTP 201
    
    NOTE: This response-body changed because I 
    changed the previous implemention to 
    include the image upload.
```

## Retrieve Drone Item:
GET http://localhost:8080/check-items/{drone-serial-number}
```
    ResponseBody: {
    "id": 1,
    "serialNumber": "2499023499863",
    "model": "Lightweight",
    "weight": 500,
    "batteryCapacity": 80,
    "state": "LOADED",
    "medications": [
        {
            "id": 1,
            "name": "MALARIA_223",
            "weight": 30,
            "code": "CODEFORTHIS",
            "image": "imageurl.com"
        }
    ]
}
    ResponseCode: HTTP 302
```

## Retrieve Available Drones:
GET http://localhost:8080/available-drones
```
    ResponseBody: [
    {
        "id": 1,
        "serialNumber": "2499023499863",
        "model": "Lightweight",
        "weight": 500,
        "batteryCapacity": 80,
        "state": "LOADED",
        "medications": [
            {
                "id": 1,
                "name": "MALARIA_223",
                "weight": 30,
                "code": "CODEFORTHIS",
                "image": "imageurl.com"
            }
        ]
    }
]
    ResponseCode: HTTP 302
```

## Retrieve Drones Battery Level:
GET http://localhost:8080/battery-level/{drone-serial-number}
```
    ResponseBody: 80
    ResponseCode: HTTP 302
```

## DataBase Models
**Drone**
```
    - id
    - serial number (100 characters max);
    - model (Lightweight, Middleweight, Cruiserweight, Heavyweight)
    - weight limit (500gr max);
    - battery capacity (percentage);
    - state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)
    - medication
```
**Medication**
```
    - id
    - name (allowed only letters, numbers, ‘-‘, ‘_’);"
    - weight;
    - code (allowed only upper case letters, underscore and numbers);
    - imageModel (picture of the medication case).
```

**ImageModel**
```
    - id
    - name
    - type
    - picBytes
```

## Functionalities
```
    - registering a drone;
    - loading a drone with medication items;
    - checking loaded medication items for a given drone; 
    - checking available drones for loading;
    - check drone battery level for a given drone;
```

## Postman Collection
```
www.postman.com/collections/d3fb92112b6018372a98
```