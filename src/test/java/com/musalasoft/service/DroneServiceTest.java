package com.musalasoft.service;

import com.musalasoft.dto.DroneDTO;
import com.musalasoft.dto.MedicationDTO;
import com.musalasoft.exception.DroneException;
import com.musalasoft.model.Drone;
import com.musalasoft.model.Medication;
import com.musalasoft.model.Model;
import com.musalasoft.model.State;
import com.musalasoft.repository.DroneRepository;
import com.musalasoft.repository.MedicationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DroneServiceTest {

    ModelMapper modelMapper = new ModelMapper();

    @InjectMocks
    DroneServiceImpl droneService = new DroneServiceImpl(modelMapper);

    @Mock
    private DroneRepository droneRepository;

    @Mock
    private MedicationRepository medicationRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void testDroneCanBeRegistered() throws DroneException{
        DroneDTO droneDTO = new DroneDTO();
        droneDTO.setState(State.IDLE);
        droneDTO.setModel(Model.Lightweight);
        droneDTO.setWeight(100);
        droneDTO.setSerialNumber("0987654321");
        droneDTO.setBatteryCapacity(100);

        droneService.registerDrone(droneDTO);
        ArgumentCaptor<Drone> droneArgumentCaptor = ArgumentCaptor.forClass(Drone.class);
        verify(droneRepository, times(1)).save(droneArgumentCaptor.capture());

        Drone capturedDrone = droneArgumentCaptor.getValue();
        assertThat(capturedDrone.getSerialNumber()).isEqualTo(droneDTO.getSerialNumber());
    }

    @Test
    void registerDroneThrowsExceptionWhenSerialNumberAlreadyExists() throws DroneException{
        String droneSerialNumber ="0987654321";
        DroneDTO droneDTO = new DroneDTO();
        droneDTO.setSerialNumber(droneSerialNumber);
        droneDTO.setState(State.IDLE);
        droneDTO.setModel(Model.Lightweight);
        droneDTO.setWeight(100);
        droneDTO.setBatteryCapacity(100);

        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);


        when(droneRepository.findBySerialNumber(droneDTO.getSerialNumber())).thenReturn(drone);

        assertThrows(DroneException.class, () -> droneService.registerDrone(droneDTO));
    }

    @Test
    void registerDroneThrowsExceptionWhenWeightIsBiggerThan500() throws DroneException{
        String droneSerialNumber ="0987654321";
        DroneDTO droneDTO = new DroneDTO();
        droneDTO.setSerialNumber(droneSerialNumber);
        droneDTO.setState(State.IDLE);
        droneDTO.setModel(Model.Lightweight);
        droneDTO.setWeight(600);
        droneDTO.setBatteryCapacity(100);

        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);
        when(droneRepository.findBySerialNumber(droneDTO.getSerialNumber())).thenReturn(drone);

        assertThrows(DroneException.class, () -> droneService.registerDrone(droneDTO));
    }

    @Test
    void registerDroneThrowsExceptionWhenBatteryCapacityIsBiggerThan100() throws DroneException{
        String droneSerialNumber ="0987654321";
        DroneDTO droneDTO = new DroneDTO();
        droneDTO.setSerialNumber(droneSerialNumber);
        droneDTO.setState(State.IDLE);
        droneDTO.setModel(Model.Lightweight);
        droneDTO.setWeight(100);
        droneDTO.setBatteryCapacity(200);

        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);
        when(droneRepository.findBySerialNumber(droneDTO.getSerialNumber())).thenReturn(drone);

        assertThrows(DroneException.class, () -> droneService.registerDrone(droneDTO));
    }

    @Test
    void registerDroneThrowsExceptionWhenBatteryCapacityIsLessThan1() throws DroneException{
        String droneSerialNumber ="0987654321";
        DroneDTO droneDTO = new DroneDTO();
        droneDTO.setSerialNumber(droneSerialNumber);
        droneDTO.setState(State.IDLE);
        droneDTO.setModel(Model.Lightweight);
        droneDTO.setWeight(100);
        droneDTO.setBatteryCapacity(0);

        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);
        when(droneRepository.findBySerialNumber(droneDTO.getSerialNumber())).thenReturn(drone);

        assertThrows(DroneException.class, () -> droneService.registerDrone(droneDTO));
    }

    @Test
    void testDroneCanBeLoaded()throws DroneException{
        String droneSerialNumber = "0987654321";
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);

        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setWeight(100);
//        medicationDTO.setImage("imageUrl");
        medicationDTO.setName("MEDICATIONNAME");
        medicationDTO.setCode("MEDICATION_CODE");

        when(droneRepository.findBySerialNumber(droneSerialNumber)).thenReturn(drone);
        droneService.loadDrone(droneSerialNumber, medicationDTO);
        ArgumentCaptor<Drone> droneArgumentCaptor = ArgumentCaptor.forClass(Drone.class);
        ArgumentCaptor<Medication> medicationArgumentCaptor = ArgumentCaptor.forClass(Medication.class);

        verify(droneRepository, times(1)).save(droneArgumentCaptor.capture());
        verify(medicationRepository, times(1)).save(medicationArgumentCaptor.capture());

        Medication captorMedication = medicationArgumentCaptor.getValue();
        Drone capturedDrone = droneArgumentCaptor.getValue();

//        assertThat(captorMedication.getImage()).isEqualTo(medicationDTO.getImage());
        assertThat(capturedDrone.getMedications()).isNotNull();
    }

    @Test
    void testLoadDroneThrowsDroneException() throws DroneException{
        String droneSerialNumber = "0987654321";
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber("123456789");

        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setWeight(100);
//        medicationDTO.setImage("imageUrl");
        medicationDTO.setName("MEDICATIONNAME");
        medicationDTO.setCode("MEDICATION_CODE");

        when(droneRepository.findBySerialNumber(drone.getSerialNumber())).thenReturn(drone);
        droneService.loadDrone(drone.getSerialNumber(), medicationDTO);
        ArgumentCaptor<Drone> droneArgumentCaptor = ArgumentCaptor.forClass(Drone.class);
        ArgumentCaptor<Medication> medicationArgumentCaptor = ArgumentCaptor.forClass(Medication.class);

        verify(droneRepository, times(1)).save(droneArgumentCaptor.capture());
        verify(medicationRepository, times(1)).save(medicationArgumentCaptor.capture());
        assertThrows(DroneException.class, () -> droneService.loadDrone(droneSerialNumber, medicationDTO));
    }

    @Test
    void testGivenDroneSerialNumberWeReturnTheContent() throws DroneException {
        String droneSerialNumber = "0987654321";
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);

        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setWeight(100);
//        medicationDTO.setImage("imageUrl");
        medicationDTO.setName("MEDICATIONNAME");
        medicationDTO.setCode("MEDICATION_CODE");
        drone.setState(State.LOADED);

        when(droneRepository.findBySerialNumber(droneSerialNumber)).thenReturn(drone);
        droneService.loadDrone(droneSerialNumber, medicationDTO);
        droneService.checkDroneItems(droneSerialNumber);
        ArgumentCaptor<Drone> droneArgumentCaptor = ArgumentCaptor.forClass(Drone.class);
        ArgumentCaptor<Medication> medicationArgumentCaptor = ArgumentCaptor.forClass(Medication.class);


        verify(droneRepository, times(1)).save(droneArgumentCaptor.capture());
        verify(medicationRepository, times(1)).save(medicationArgumentCaptor.capture());

        Medication captorMedication = medicationArgumentCaptor.getValue();
        Drone capturedDrone = droneArgumentCaptor.getValue();

        assertThat(capturedDrone.getMedications().size()).isEqualTo(1);
    }

    @Test
    void testBatteryLevelCanBeChecked() throws DroneException {
        String droneSerialNumber = "0987654321";
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);

        when(droneRepository.findBySerialNumber(droneSerialNumber)).thenReturn(drone);
        droneService.checkBatteryLevel(drone.getSerialNumber());

        assertThat(drone.getBatteryCapacity()).isEqualTo(100);
    }

    @Test
    void testExceptionIsThrownWhenWrongSerialNumberNotFoundBatteryLevelCanBeChecked() throws DroneException {
        String droneSerialNumber = "0987654321";
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);


        assertThrows(DroneException.class, () -> droneService.checkBatteryLevel(any()));
    }

    @Test
    void checkAndReturnAvailableDrone(){
        String droneSerialNumber = "0987654321";
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setId(7L);
        drone.setSerialNumber(droneSerialNumber);

        List<Drone> drones = new ArrayList<>();
        drones.add(drone);

        when(droneRepository.findAll()).thenReturn(drones);


        assertEquals(1, droneService.checkAvailableDrone().size());
    }

}
