package com.musalasoft.controller;

import com.musalasoft.dto.DroneDTO;
import com.musalasoft.dto.MedicationDTO;
import com.musalasoft.exception.DroneException;
import com.musalasoft.model.Drone;
import com.musalasoft.model.ImageModel;
import com.musalasoft.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.Set;

@RestController
public class DispatchController {
    @Autowired
    private DroneService droneService;

    @PostMapping("/register-drone")
    public ResponseEntity<?> registerDrone(@RequestBody DroneDTO droneDTO){
        try{
            Drone newDrone = droneService.registerDrone(droneDTO);
            return new ResponseEntity<>(newDrone, HttpStatus.CREATED);
        }catch (DroneException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/load-drone/{droneSerialNumber}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> loadDrone(@PathVariable String droneSerialNumber,
                                       @RequestPart("medicationDTO") MedicationDTO medicationDTO,
                                       @RequestPart("imageFile") MultipartFile[] multipartFiles){

        try {
            Set<ImageModel> images = uploadImage(multipartFiles);
            medicationDTO.setMedicationImages(images);
            Drone droneToLoad = droneService.loadDrone(droneSerialNumber, medicationDTO);
            return new ResponseEntity<>(droneToLoad, HttpStatus.CREATED);
        }catch (DroneException | Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/check-items/{droneSerialNumber}")
    public ResponseEntity<?> checkDroneItems(@PathVariable String droneSerialNumber){
        try{
            return new ResponseEntity<>(droneService.checkDroneItems(droneSerialNumber), HttpStatus.FOUND);
        }catch (DroneException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/available-drones")
    public ResponseEntity<?> checkAvailableDrones(){
        return new ResponseEntity<>(droneService.checkAvailableDrone(), HttpStatus.FOUND);
    }

    @GetMapping("/battery-level/{droneSerialNumber}")
    public ResponseEntity<?> checkBatteryLevel(@PathVariable String droneSerialNumber){
        try {
            return new ResponseEntity<>(droneService.checkBatteryLevel(droneSerialNumber), HttpStatus.FOUND);
        }catch (DroneException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private Set<ImageModel> uploadImage(MultipartFile[] multipartFiles) throws Exception {
        Set<ImageModel> imageModels = new HashSet<>();

        for (MultipartFile file : multipartFiles){
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            if (fileName.contains("..")) {
                throw new Exception("Filename contains invalid path sequence" + fileName);
            }
            ImageModel imageModel = new ImageModel(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getBytes()
            );
            imageModels.add(imageModel);
        }
        return imageModels;
    }
}
