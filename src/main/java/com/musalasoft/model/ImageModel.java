package com.musalasoft.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "image_model")
@NoArgsConstructor
public class ImageModel {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String type;
    @Column(length = 50000000)
    private byte[] picBytes;

    public ImageModel(String name, String type, byte[] picBytes) {
        this.name = name;
        this.type = type;
        this.picBytes = picBytes;
    }
}
