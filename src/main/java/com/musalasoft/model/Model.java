package com.musalasoft.model;

public enum Model {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}
