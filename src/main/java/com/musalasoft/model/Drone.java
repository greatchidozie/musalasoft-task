package com.musalasoft.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Builder
@Data
@Entity(name = "drone")
@NoArgsConstructor
@AllArgsConstructor
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Size(max = 100)
    private String serialNumber;
    private Model model;
    private Integer weight;
    private Integer batteryCapacity;
    private State state;
    @OneToMany
    private List<Medication> medications;

    public void addMedications(Medication... newMedications){
        if (medications == null){
            this.medications = new ArrayList<>();
        }
        this.medications.addAll(Arrays.asList(newMedications));
    }
}
