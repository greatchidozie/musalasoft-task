package com.musalasoft.dto;

import com.musalasoft.model.Medication;
import com.musalasoft.model.Model;
import com.musalasoft.model.State;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Data
public class DroneDTO {
    @Size(max = 100)
    private String serialNumber;
    private Model model;
    private Integer weight;
    private Integer batteryCapacity;
    private State state;
    @OneToMany
    private List<Medication> medications;
}
