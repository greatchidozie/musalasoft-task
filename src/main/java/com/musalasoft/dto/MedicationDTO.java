package com.musalasoft.dto;

import com.musalasoft.model.ImageModel;
import lombok.Data;

import java.util.Set;

@Data
public class MedicationDTO {
        private String name;
        private Integer weight;
        private String code;
        private Set<ImageModel> medicationImages;
//        private String image;
    }
