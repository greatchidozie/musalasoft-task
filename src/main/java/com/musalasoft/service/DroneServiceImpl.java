package com.musalasoft.service;

import com.musalasoft.dto.DroneDTO;
import com.musalasoft.dto.MedicationDTO;
import com.musalasoft.exception.DroneException;
import com.musalasoft.model.Drone;
import com.musalasoft.model.Medication;
import com.musalasoft.model.State;
import com.musalasoft.repository.DroneRepository;
import com.musalasoft.repository.MedicationRepository;
import com.musalasoft.util.DroneBuilderClass;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class DroneServiceImpl implements DroneService{

    @Autowired
    private DroneRepository droneRepository;
    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void init(){
        DroneBuilderClass drones = new DroneBuilderClass();
        droneRepository.saveAll(drones.registerDrones());
    }

    public DroneServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /*
    * I am assuming here that each drone has a unique serial number as in real
    * life, and a drone can't be registered on our platform more than once.
    * */
    @Override
    public Drone registerDrone(DroneDTO droneDTO) throws DroneException {
        Drone newDrone = new Drone();
        Optional<Drone> optionalDrone = Optional.ofNullable(droneRepository.findBySerialNumber(droneDTO.getSerialNumber()));
        if(optionalDrone.isPresent()){
            throw new DroneException("Drone with serial number already exists");
        }
        if (droneDTO.getBatteryCapacity() > 100 || droneDTO.getBatteryCapacity() < 1 ){
            log.info("Specified battery is bigger than 100 or less than 1");
            throw new DroneException("Specified battery is bigger than 100 or less than 1");
        }
        if (droneDTO.getWeight() > 500){
            log.info("Specified weight is bigger than 500");
            throw new DroneException("Specified weight is bigger than 500");
        }
        /*
        * Model-mapper maps our Dto to the entity object.
        * */
        modelMapper.map(droneDTO, newDrone);
        log.info("Drone with {} serial number, registered", newDrone.getSerialNumber());
        return droneRepository.save(newDrone);
    }


    /*The method used for loading a drone first checks for an available drone
    * If th
    * */
    @Override
    public Drone loadDrone(String droneSerialNumber, MedicationDTO medicationDTO) throws DroneException {
        Drone droneToLoad = droneRepository.findBySerialNumber(droneSerialNumber);
        Medication newMedication = new Medication();

        if (droneToLoad == null){
            log.info("Drone with provided serial number not found");
            throw new DroneException("Drone with provided serial number not found");
        }
        validateMedication(medicationDTO);
        validateDrone(droneToLoad);

            modelMapper.map(medicationDTO, newMedication);
            Medication savedMedication = medicationRepository.save(newMedication);
            droneToLoad.addMedications(savedMedication);
            droneToLoad.setState(State.LOADED);
            return droneRepository.save(droneToLoad);
    }

    @Override
    public Drone checkDroneItems(String droneSerialNumber) throws DroneException {
        // This method returns a drone,
        // and it's item when a drone serial number is given.
        Drone drone = droneRepository.findBySerialNumber(droneSerialNumber);
        if(drone.getState() == State.LOADED || drone.getState() == State.DELIVERING){
            return drone;
        }else {
            log.info("Drone doesn't have any item at the moment");
            throw new DroneException("Drone doesn't have any item");
        }
    }

    @Override
    public List<Drone> checkAvailableDrone() {
        //This method checks the list of drones we have and returns only the ones that's loaded.

        List<Drone> availableDrones = droneRepository.findAll();
        /* Here I am assuming that Idle, Delivered and Returning
         are states where we can call a drone and load more medications to it.
         Also, I checked battery capacity and marked it unavailable if the battery is less than 25.
         */

        availableDrones.removeIf(drone -> drone.getState() != State.IDLE && drone.getState() != State.DELIVERED
                && drone.getState() != State.RETURNING && drone.getBatteryCapacity() < 25);
        return availableDrones;
    }

    /*
    The Method Below.
    Given a drone serial number, I return the current battery capacity of the drone.
    * */
    @Override
    public Integer checkBatteryLevel(String droneSerialNumber) throws DroneException {
        Drone drone = droneRepository.findBySerialNumber(droneSerialNumber);
        if (drone == null){
            throw new DroneException("Drone with "+ droneSerialNumber +" serial number not found");
        }
        log.info("The battery capacity of drone with {} serial number is {}", droneSerialNumber, drone.getBatteryCapacity());
        return drone.getBatteryCapacity();
    }

    private boolean validateMedicationCode(String medicationCode){
        String allowedPattern = "^[A-Z0-9_]*$";
        Pattern pattern = Pattern.compile(allowedPattern);
        Matcher matcher = pattern.matcher(medicationCode);
        return matcher.matches();
    }

    private boolean validateMedicationName(String medicationName){
        String allowedPattern = "^[A-Za-z0-9_-]*$";
        Pattern pattern = Pattern.compile(allowedPattern);
        Matcher matcher = pattern.matcher(medicationName);
        return matcher.matches();
    }

    private void validateMedication(MedicationDTO medicationDTO) throws DroneException {
        /*
        Method makes sure medication meets the specified
        requirements before getting loaded.
        */

        if(!validateMedicationName(medicationDTO.getName())){
            log.info("Name can only consist of letters, numbers, dash and/or underscore");
            throw new DroneException("Name can only consist of letters, numbers, dash and/or underscore");
        }

        if (!validateMedicationCode(medicationDTO.getCode())){
            throw new DroneException("Code can only only upper case letters, underscore and/or numbers");
        }

        if (medicationDTO.getWeight() > 500){
            log.info("The weight is more that specified maximum");
            throw new DroneException("Weight more than specified maximum");
        }
    }

    private void validateDrone(Drone droneToLoad) throws DroneException {
        /*
        Method makes sure drone meets the specified
        requirements before getting loaded.
        */
        if (droneToLoad.getState() != State.LOADED &&
                droneToLoad.getState() != State.IDLE &&
                droneToLoad.getState() != State.RETURNING){
            log.info("We don't have any available drone for loading now please check back later");
            throw new DroneException("Drone not available for loading, try later");
        }

        if(droneToLoad.getBatteryCapacity() < 25){
            log.info("You can't load the selected drone as the battery is low");
            throw new DroneException("The drown does not have enough battery for this trip");
        }
    }
}
