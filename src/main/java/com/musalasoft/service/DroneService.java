package com.musalasoft.service;

import com.musalasoft.dto.DroneDTO;
import com.musalasoft.dto.MedicationDTO;
import com.musalasoft.exception.DroneException;
import com.musalasoft.model.Drone;

import java.util.List;

public interface DroneService {
    Drone registerDrone(DroneDTO droneDTO) throws DroneException;
    Drone loadDrone(String droneSerialNumber, MedicationDTO medicationDTO) throws DroneException;
    Drone checkDroneItems(String droneSerialNumber) throws DroneException;
    List<Drone> checkAvailableDrone();
    Integer checkBatteryLevel(String droneSerialNumber) throws DroneException;
}
