package com.musalasoft.util;

import com.musalasoft.model.Drone;
import com.musalasoft.repository.DroneRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

@Slf4j
@Configuration
@EnableScheduling
public class BatteryLevelTask {

    @Autowired
    private DroneRepository droneRepository;
    /*
    The Job gets battery life every 10 seconds,
    his can be modified by increasing the fixed
    delay time specified below.
    */
    @Scheduled(fixedDelay = 5000)
    public void checkBatteryPercentage(){

        List<Drone> drones = droneRepository.findAll();
        for (Drone drone : drones){
            log.info("Drone battery level for drone with serial number {} is {}", drone.getSerialNumber(), drone.getBatteryCapacity());
        }
    }
}
