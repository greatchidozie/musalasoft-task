package com.musalasoft.util;


import com.musalasoft.model.Drone;
import com.musalasoft.model.Model;
import com.musalasoft.model.State;

import java.util.ArrayList;
import java.util.List;

/*
This class will be used to create Drones immediately
the service is started
* */
public class DroneBuilderClass {

    public List<Drone> registerDrones(){
        List<Drone> drones = new ArrayList<>();

        Drone drone = new Drone();
        drone.setSerialNumber("0987654321");
        drone.setBatteryCapacity(100);
        drone.setModel(Model.Lightweight);
        drone.setState(State.IDLE);
        drone.setWeight(400);

        Drone drone1 = new Drone();
        drone1.setSerialNumber("0987321654");
        drone1.setBatteryCapacity(45);
        drone1.setModel(Model.Middleweight);
        drone1.setState(State.RETURNING);
        drone1.setWeight(330);


        Drone drone3 = new Drone();
        drone3.setSerialNumber("5012349876");
        drone3.setBatteryCapacity(70);
        drone3.setModel(Model.Cruiserweight);
        drone3.setState(State.LOADED);
        drone3.setWeight(400);

        Drone drone2 = new Drone();
        drone2.setSerialNumber("5409876321");
        drone2.setBatteryCapacity(50);
        drone2.setModel(Model.Heavyweight);
        drone2.setState(State.LOADING);
        drone2.setWeight(500);

        drones.add(drone);
        drones.add(drone1);
        drones.add(drone2);
        drones.add(drone3);

        return drones;
    }
}
